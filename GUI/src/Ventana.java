import java.awt.EventQueue;

import javax.swing.*;

public class Ventana {

	private JFrame frame;    //jgframe es la clase que abre la ventana, frame es la base de la aplicacion
	private JLabel label1;   //label es un texto en la pantalla
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana window = new Ventana();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ventana() {
		initialize();
		frame.setLayout(null);              //esto es para indicar al objeto de jframe que usare posicionamiento absoluto en los controles visuales
		label1 = new JLabel("Hola mundo");  
		label1.setBounds(10, 20, 40, 20);
		frame.add(label1);     //a�ade el label a la ventana
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
